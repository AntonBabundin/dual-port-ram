// Design Name : dual_port_synch_ram
// File Name   : tb_dual_port_synch_ram.v
// Function    : testbench for 1024 x 16 dual-port single-frequency memory, with input registers
// Coder       : Babundin Anton
`timescale 100 ps/ 1 ps

module tb_dual_port_synch_ram();

  reg                  clk; 
  reg           reset_rg_a;  
  reg           reset_rg_b; 
  reg        data_a_enable; 
  reg        data_b_enable; 
  reg       write_enable_a;
  reg       write_enable_b;
  reg [15:0]        data_a;
  reg [15:0]        data_b;
  reg [5:0]      address_a;
  reg [5:0]      address_b; 
  
  wire [15:0]  data_a_out;
  wire [15:0]  data_b_out;

  dual_port_synch_ram  i1 (.clk(clk), .reset_rg_a(reset_rg_a), .reset_rg_b(reset_rg_b), .data_a_enable(data_a_enable), .data_b_enable(data_b_enable), .write_enable_a(write_enable_a), .write_enable_b(write_enable_b), .data_a(data_a), .data_b(data_b), .address_a(address_a), .address_b(address_b), .data_a_out(data_a_out), .data_b_out(data_b_out));

  initial
    begin
      clk = 0;
        
      #20
      reset_rg_a = 1'b1;
      reset_rg_b = 1'b1;
        

      #20
      reset_rg_a = 1'b0;
      reset_rg_b = 1'b0;

      #20
      data_a_enable = 1'b1;
      write_enable_a = 1'b1;
      address_a = 6'h3F;
      data_a = 16'hFFFF;
      data_b_enable = 1'b1;
      write_enable_b = 1'b1;
      address_b = 6'h7;
      data_b = 16'hFF;
        

      #20
      data_a_enable = 1'b0;
      write_enable_a = 1'b0;
      address_a = 6'h0;
      data_a = 16'h0;
      data_b_enable = 1'b0;
      write_enable_b = 1'b0;
      address_b = 6'h0;
      data_b = 16'h0;
    
      #20
      address_a = 6'h3F;
      address_b = 6'h7;
        
      #20
      $stop; 
     end 
     
  always
    begin
      #5
      clk = !clk;
    end
endmodule 