// Design Name : dual_port_synch_ram
// File Name   : dual_port_synch_ram.v
// Function    : 1024 x 16 dual-port single-frequency memory, with input registers
// Coder       : Babundin Anton

module dual_port_synch_ram
(
  input wire                   clk,// Clock
  input wire            reset_rg_a,// Reset for register on Data A line
  input wire            reset_rg_b,// Reset for register on Data B line
  input wire         data_a_enable,// Enable for register on Data A line
  input wire         data_b_enable,// Enable for register on Data B line
  input wire        write_enable_a,// Enable for write in RAM on Data A line
  input wire        write_enable_b,// Enable for write in RAM on Data B line
  input wire [15:0]         data_a,// Data A
  input wire [15:0]         data_b,// Data B
  input wire [5:0]       address_a,// Address for Data A
  input wire [5:0]       address_b,// Address for Data B
  
  output reg [15:0]     data_a_out,// Data A out from RAM
  output reg [15:0]     data_b_out // Data B out from RAM
  
);  
  reg [15:0] ram[63:0];// Declare the RAM
  
  wire [15:0] data_out_reg_a;// Data A from register to RAM
  wire [15:0] data_out_reg_b;// Data B from register to RAM
  
  rg rg_data_a (.clk(clk), .reset(reset_rg_a), .enable(data_a_enable), .data_in(data_a), .data_out(data_out_reg_a));// Declare register on input Data A
  rg rg_data_b (.clk(clk), .reset(reset_rg_b), .enable(data_b_enable), .data_in(data_b), .data_out(data_out_reg_b));// Declare register on input Data B

  always @(posedge clk)
    begin
      if (write_enable_a)// Write Data A
        begin 
          ram[address_a] <= data_out_reg_a;  
          data_a_out <= data_out_reg_a;
        end
      else
        begin
          data_a_out <= ram[address_a]; //Read Data A
        end  
    end
     
  always @(posedge clk)
    begin
      if (write_enable_b)// Write Data B
        begin 
          ram[address_b] <= data_out_reg_b;  
          data_b_out <= data_out_reg_b;
        end
      else
        begin
          data_b_out <= ram[address_b];// Read Data B
        end
    end
     
endmodule 