// Design Name : dual_port_synch_ram
// File Name   : rg.v
// Function    : Simple regster with synch reset
// Coder       : Babundin Anton

module rg
(
  input wire            clk,
  input wire          reset,
  input wire         enable,
  input wire [15:0] data_in,
  
  output reg [15:0] data_out
);

  always @(posedge clk)
    begin
      if(reset)
        begin
          data_out <= 16'h0;
        end
      else
	    begin
          if(enable)
            begin
              data_out <= data_in;
            end
	    end
    end

endmodule